import sys

class Dummy(object):
    def __init__(self, *args):
        pass

    def debug(self, *args):
        pass

class Logger(object):
    def __init__(self, filename):
        self.__file = filename

    def debug(self, message):
        with file(self.__file, "a") as f:
            f.write(str(message) + "\n")

Log = None
def getLogger():
    global Log
    if not Log:
        if len(sys.argv) > 1:
            if sys.argv[1] == "DEBUG":
                Log = Logger("/tmp/ants.log")
            else:
                Log = Dummy()
        else:
            Log = Dummy()
    return Log

