import random
import sys
import engine
import traceback

from engine import UNSEEN, LAND, FOOD, WATER, MY_HILL, ENEMY_HILL, DEAD_ANT, MY_ANT, ENEMY_ANT

class EngineInterface(object):
    def setup(self):
        """parse initial input and setup starting engine state"""
        self.engine = engine.Engine()
        while True:
            line = sys.stdin.readline().strip().lower()
            engine.getLogger().debug(line)
            if line == "ready":
                break
            if len(line) > 0:
                tokens = line.split()
                key = tokens[0]
                if key == 'cols':
                    self.engine.cols = int(tokens[1])
                elif key == 'rows':
                    self.engine.rows = int(tokens[1])
                elif key == 'player_seed':
                    random.seed(int(tokens[1]))
                elif key == 'turntime':
                    self.engine.turntime = int(tokens[1])
                elif key == 'loadtime':
                    self.engine.loadtime = int(tokens[1])
                elif key == 'viewradius2':
                    self.engine.viewradius2 = int(tokens[1])
                elif key == 'attackradius2':
                    self.engine.attackradius2 = int(tokens[1])
                elif key == 'spawnradius2':
                    self.engine.spawnradius2 = int(tokens[1])
                elif key == 'turns':
                    self.engine.turns = int(tokens[1])
        squares = self.engine.rows * self.engine.cols
        self.engine.map_data.extend([UNSEEN for i in range(squares)])
        self.engine.do_setup()
        self.end_turn()

    def update(self):
        my_ant_list = {}
        my_old_ant_list = self.engine.my_ants
        enemy_ant_list = []
        food_list = {}
        my_hill_list = []
        enemy_hill_list = []
        dead_ants = []
        old_orders = self.engine.orders

        def _update_water(idx):
            self.engine.map_data[idx] = WATER
            
        def _update_land(idx):
            self.engine.map_data[idx] = LAND
            pass

        def _update_food(idx):
            self.engine.map_data[idx] = FOOD
            food_list[idx] = 1

        def _update_ants(idx, player):
            if int(player):
                self.engine.map_data[idx] = ENEMY_ANT
                enemy_ant_list.append(idx)
            else:
                self.engine.map_data[idx] = MY_ANT
                if old_orders.has_key(idx) and my_old_ant_list.has_key(old_orders[idx]):
                    old_ant = my_old_ant_list.pop(old_orders[idx])
                    old_ant.id = idx
                    my_ant_list[idx] = old_ant
                elif my_old_ant_list.has_key(idx) and (my_old_ant_list[idx].id == idx):
                    my_ant_list[idx] = my_old_ant_list.pop(idx)
                else:
                    my_ant_list[idx] = engine.Ant(idx)


        def _update_dead_ants(idx, player):
            if self.engine.map_data[idx] != ENEMY_HILL:
                _update_land(idx)

        def _update_hills(idx, player):
            if int(player):
                self.engine.map_data[idx] = ENEMY_HILL
                enemy_hill_list.append(idx)
                self.engine.hill_rewards[idx] = {}
            else:
                self.engine.map_data[idx] = MY_HILL
                my_hill_list.append(idx)

        update_map = {
                'w' : _update_water,
                'f' : _update_food,
                'a' : _update_ants,
                'd' : _update_dead_ants,
                'h' : _update_hills
        }

        cols = self.engine.cols
        engine.getLogger().debug("Updating map...")
        while True:
            line = sys.stdin.readline().strip()
            engine.getLogger().debug(line)
            if line == "go":
                break
            tokens = line.split()
            if tokens[0].lower() == "turn":
                self.engine.turn = int(tokens[1])
                continue
            if len(tokens) < 3:
                continue
            idx = int(tokens[1])*cols + int(tokens[2])
            update_map.get(tokens[0], _update_land)(idx, *tokens[3:])

        self.engine.my_ants.clear()
        self.engine.my_ants.update(my_ant_list)
        self.engine.enemy_ant_list = enemy_ant_list
        self.engine.new_food = food_list
        self.engine.my_hills = my_hill_list
        self.engine.enemy_hills = enemy_hill_list
        self.engine.dead_ants.update(dead_ants)
#self.end_turn()

    def end_turn(self):
        sys.stdout.write("go\n")
        sys.stdout.flush()

    def run(self):
        self.setup()
        while True:
            try:
                self.update()
                self.engine.do_turn()
                self.end_turn()
            except EOFError:
                break
            except KeyboardInterrupt:
                raise
            except:
                engine.getLogger().debug(traceback.format_exc())
                traceback.print_exc(file=sys.stderr)
                sys.stderr.flush()

def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == "PROFILE":
            import cProfile
            cProfile.run("EngineInterface().run()", "/tmp/ants.profile")
    EngineInterface().run()

if __name__ == '__main__':
    main()
