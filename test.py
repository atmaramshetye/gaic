import os
import sys
import pstats

def command_str():
    turns = 100
    enemy_count = 3
    map = "maps/random_walk/random_walk_%02dp_02.map" % (enemy_count+1)
#map = "maps/multi_hill_maze/maze_%02dp_01.map" % (enemy_count+1)
#map = "maps/maze/maze_%02dp_01.map" % (enemy_count+1)
#map = "maps/cell_maze/cell_maze_p%02d_01.map" % (enemy_count+1)
    bot = '"python ../Benchmark/bot.py"'
#bot = '"python ../Python2/oldbot.py"'
    #if enemy_count < 3:
    if enemy_count < 6:
        bots = " ".join([bot for i in range(enemy_count)])
    else:
        bots = " ".join(['"python sample_bots/python/%s"' %(i) for i in ["HunterBot.py", "GreedyBot.py"]])
        bots += " " + " ".join([bot for i in range(enemy_count-2)])
    my_bot = 'python ../Python/MyBot.py DEBUG'
#my_bot = 'python ../Python/MyBot.py PROFILE'
    my_bot = '../C/MyBot'

    return './playgame.py --player_seed 42 --end_wait=0.25 --verbose --log_dir game_logs --turns %(turns)d --map_file %(map)s "$@" %(bots)s "%(my_bot)s"' % locals()
    #return './playgame.py --player_seed 42 --end_wait=0.25 --verbose --log_dir game_logs --turns %(turns)d --map_file %(map)s "$@" "%(my_bot)s" %(bots)s' % locals()

def print_profiler_output(filename):
    
    p = pstats.Stats(filename).strip_dirs().sort_stats('time')
    p.print_stats()


os.system("rm -f /tmp/ants.log")
os.system("rm -f ants.profile")
os.chdir("C")
os.system("make")
os.chdir("../tools")
#os.chdir("tools")
print command_str()
os.system(command_str())
#print_profiler_output("/tmp/ants.profile")
