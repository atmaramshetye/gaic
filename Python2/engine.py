import sys
import math
import time
import random

class Dummy(object):
    def __init__(self, *args):
        pass

    def debug(self, *args):
        pass

class Logger(object):
    def __init__(self, filename):
        self.__file = filename

    def debug(self, message):
        with file(self.__file, "a") as f:
            f.write(str(message) + "\n")

log = None
def getLogger():
    return Dummy()
    global log
    if not log:
        log = Logger("/tmp/ants.log")
    return log

IDLE = 0
DRIVEN_OUT = 1
GATHERING = 2
EXPLORING = 4
DEFENDING= 8
ATTACKING = 16
RAZING = 32
WAITING = 64


UNSEEN = 1
WATER = 2
LAND = 4
FOOD = 8
MY_HILL = 16
ENEMY_HILL = 32
DEAD_ANT = 64
MY_ANT = 128
ENEMY_ANT = 256

UNPASSABLE = (WATER | FOOD | MY_ANT)
JACKPOT = (ENEMY_ANT)

MY_ANT_BATTLE_SCORE = 600
ENEMY_ANT_BATTLE_SCORE = -1000

CLOCKWISE = ('e', 's', 'w', 'n')
ANTICLOCKWISE = ('e', 'n', 'w', 's')

FOOD_REWARDS = {
        UNSEEN : 0,
        WATER : -1,
        LAND : 0,
        FOOD : 100,
        MY_HILL : -2,
        ENEMY_HILL : 1000
        }

HILL_REWARDS = {
        ENEMY_ANT : 10,
        ENEMY_HILL : 9999
        }

DEFENSE_REWARDS = {
        MY_ANT : 10,
        MY_HILL : 9999
        }

def abs(val):
    if val < 0:
        return (-val)
    return val

class Ant(object):
    def __init__(self, loc):
        self.id = loc
        self.state = IDLE
        self.prev_dir = None
        self.blacklist = {}
        self.tgt = -1
        self.steps = -1

    def __str__(self):
        return "Ant (id: %d, state: %d)" % (self.id, self.state)

class Engine(object):
    def __init__(self):
        self.cols = 0
        self.rows = 0
        self.turntime = 0
        self.loadtime = 0
        self.viewradius2 = 0
        self.attackradius2 = 0
        self.spawmradius2 = 0
        self.turns = 0
        self.data = []
        self.my_ants = {}
        self.enemy_ants = []
        self.my_hills = []
        self.enemy_hills = []
        self.food = {}
        self.dead_ants = {}
        self.orders = {}
        self.directions = {'n' : 0, 'w' : 1, 's' : 2, 'e' : 3}
        self.turn = 0

    def do_setup(self):
        self.size = self.rows * self.cols
        self.viewradius = int(math.sqrt(self.viewradius2))
        self.scan_radius = int(math.sqrt(2*self.viewradius2))
        self.attackradius = int(math.sqrt(self.attackradius2))
        self.available_ants = {}
        self.stochasism = {
                'n' : ['n']*75 + ['e']*10 + ['w'] * 10 + ['s']*5,
                's' : ['s']*75 + ['e']*10 + ['w'] * 10 + ['n']*5,
                'e' : ['e']*75 + ['n']*10 + ['s'] * 10 + ['w']*5,
                'w' : ['w']*75 + ['n']*10 + ['s'] * 10 + ['e']*5}
        self.spiral_dirs = (('e', 'n', 'w', 's'), ('w', 'n', 's', 'e'))
        self.food_rewards = {}
        self.hill_rewards = {}
        self.enemy_ant_rewards = {}
        self.old_food = {}
        self.heuristic_score = {
                ENEMY_ANT : -10,
                MY_ANT : 5,
                ENEMY_HILL : 100
                }

    def cleanup(self):
        for loc in self.food:
            self.data[loc][0] = LAND
            self.food_rewards[loc] = 0
        for loc in self.dead_ants:
            self.data[loc][0] = LAND
        for loc in self.enemy_ants:
            self.data[loc][0] = LAND
        del self.enemy_ants[:]
        for loc in self.my_ants:
            self.data[loc][0] = LAND
        self.available_ants.clear()
        self.old_food.update(self.food)
        self.food.clear()

    def distance_sq(self, src, dest):
        x = (dest - src) % self.cols
        y = (dest - src) / self.cols

        if x > self.cols/2:
            x = self.cols - x
        if y > self.rows/2:
            y = self.rows - y

        return x*x + y*y

    def distant_location(self, loc, row_offset, col_offset):
        row, col = loc / self.cols, loc % self.cols
        new_row = (row + row_offset) % self.rows
        new_col = (col + col_offset) % self.cols
        return new_row * self.cols + new_col

    def new_loc(self, src, dir):
        if dir == 'n':
            dest = (src - self.cols) % self.size
        elif dir == 's':
            dest = (src + self.cols) % self.size
        elif dir == 'w':
            if src % self.cols:
                dest = src - 1
            else:
                dest = src + self.cols - 1
        else:
            dest = src + 1
            if not dest % self.cols:
                dest = dest - self.cols
        return dest

    def do_move(self, src, dir):
        #r = random.randint(0, 99)
        #dir = self.stochasism[dir][r]
        dest = self.new_loc(src, dir)
        if self.data[dest][0] & UNPASSABLE:
            self.my_ants[src].state = IDLE
            return 0

        if self.orders.has_key(dest):
            self.my_ants[src].state = IDLE
            return 0
        self.orders[dest] = src
        sys.stdout.write('o %s %s %s\n' % (src / self.cols, src % self.cols, dir))
        sys.stdout.flush()
        #getLogger().debug('o %s %s %s\n' % (src / self.cols, src % self.cols, dir))
        return 1

    def aim(self, src, dest):
        src_r, src_c = src / self.cols, src % self.cols
        dest_r, dest_c = dest / self.cols, dest % self.cols

        directions = []
        diff = dest - src
        # Compute North/South direction
        diff = dest_r - src_r
        if abs(diff) > self.rows/2:
            if diff > 0:
                directions.append('n')
            elif diff < 0:
                directions.append('s')
        elif diff > 0:
            directions.append('s')
        elif diff < 0:
            directions.append('n')

        # Compute East/West direction
        diff = dest_c - src_c
        if abs(diff) > self.cols/2:
            if diff > 0:
                directions.append('w')
            elif diff < 0:
                directions.append('e')
        elif diff > 0:
            directions.append('e')
        elif diff < 0:
            directions.append('w')

        return directions

    def repel(self, src, dest):
        opposites = {
            'n' : 's',
            'e' : 'w',
            'w' : 'e',
            's' : 'n'
        }

        return [opposites[i] for i in self.aim(src, dest)]

    def unblock_hills(self):
        for hill in self.my_hills:
            if self.my_ants.has_key(hill):
                for dir in self.directions:
                    if self.do_move(hill, dir):
                        self.my_ants[hill].state = DRIVEN_OUT
                        self.available_ants.pop(hill)
                        break

    def compute_qvalue(self, dest, rewards):
        directions = ANTICLOCKWISE
        computed = max([rewards.get(self.new_loc(dest, i), 0) for i in directions]) - 1
        old = rewards.get(dest, 0)
        return max(old, computed)

    def populate_max_qvalues(self, loc, radius, dir_index):
        for directions in (ANTICLOCKWISE, CLOCKWISE):
            repeat_cnt_preset = 1
            dir_change_cnt = 0
            repeat_cnt = repeat_cnt_preset
        
            dest = loc
            obj = self.data[dest][0]
            self.food_rewards[dest] = max(self.food_rewards.get(dest, 0), FOOD_REWARDS.get(obj, 0))
            #getLogger().debug("REWARD at (%d, %d)(%d): %d" % (dest/self.cols, dest%self.cols, obj, self.food_rewards[dest]))
            #getLogger().debug("======================")
            for i in range(2 * self.viewradius2):
                if not repeat_cnt:
                    dir_index = (dir_index + 1) % 4
                    dir_change_cnt += 1
                    if not dir_change_cnt % 2:
                        repeat_cnt_preset += 1
                    repeat_cnt = repeat_cnt_preset
                repeat_cnt -= 1
                dest = self.new_loc(dest, directions[dir_index])
                obj = self.data[dest][0]
                if (obj == WATER) or (obj == UNSEEN):
                    self.food_rewards[dest] = -1
                else:
                    self.food_rewards[dest] = self.compute_qvalue(dest, self.food_rewards)
                #getLogger().debug("REWARD at (%d, %d): %d" % (dest/self.cols, dest%self.cols, self.food_rewards[dest]))

    def defend(self):
        pass

    def get_circle(self, loc, radius):
        x0, y0 = loc / self.cols, loc % self.cols
        f = 1 - radius
        ddF_x = 1
        ddF_y = -2 * radius
        x = 0
        y = radius

        squares = {'n' : [], 'w' : [], 's' : [], 'e' : []}
        north = self.distant_location(loc, 0, radius)
        south = self.distant_location(loc, 0, -radius)
        east = self.distant_location(loc, radius, 0)
        west = self.distant_location(loc, -radius, 0)
        squares['n'].extend([east, north, west])
        squares['w'].extend([north, west, south])
        squares['s'].extend([west, south, east])
        squares['e'].extend([south, west, north])
     
        while x < y:
            if(f >= 0): 
                y -= 1
                ddF_y += 2
                f += ddF_y
            x += 1
            ddF_x += 2
            f += ddF_x
            ne1 = self.distant_location(loc, x, y)
            se1 = self.distant_location(loc, x, -y)
            nw1 = self.distant_location(loc, -x, y)
            sw1 = self.distant_location(loc, -x, -y)
            ne2 = self.distant_location(loc, y, x)
            se2 = self.distant_location(loc, y, -x)
            nw2 = self.distant_location(loc, -y, x)
            sw2 = self.distant_location(loc, -y, -x)

            squares['n'].extend([ne1, nw1, ne2, nw2])
            squares['w'].extend([nw1, sw1, nw2, sw2])
            squares['s'].extend([se1, sw1, se2, sw2])
            squares['e'].extend([ne1, ne2, se1, se2])
        return squares

    def get_battle_scores(self, loc):
        my_ant_score = MY_ANT_BATTLE_SCORE
        enemy_ant_score = ENEMY_ANT_BATTLE_SCORE
        scores = {
            'n' : 0,
            'w' : 0,
            's' : 0,
            'e' : 0
            }
        # Circle of same radius as the attack radius is not possible
        # as either one or none ant would survive
        my_circle = self.get_circle(loc, self.attackradius + 1)
        for dir in ('n', 'w', 's', 'e'):
            battle_score = 0
            for square in my_circle[dir]:
                if self.data[square][0] == ENEMY_ANT:
                    enemy_circle = self.get_circle(square, self.attackradius)
                    enemy_squares = enemy_circle['n'] + enemy_circle['s']
                    battle_score += enemy_ant_score
                    for enemy_square in enemy_squares:
                        # Assuming that no ant is stationary which might
                        # not always be the case
                        if self.orders.has_key(enemy_square):
                            battle_score += my_ant_score
            scores[dir] = battle_score
        return scores

    def compute_dir_scores(self, loc):
        # TODO: Mark enemy ants and keep count of how many of my ants can attack it.
        #getLogger().debug("COMPUTING DIR SCORE for location (%d, %d)" % (loc/self.cols, loc%self.cols))
        r = self.attackradius + 1
        r2 = r*r
        score = self.heuristic_score.get(self.data[loc][0], 0)
        enemy_ant_present = False
        # North, West, South, East
        scores = [0, 0, 0, 0]
        weights = [0, 0, 0, 0]
        for x in range(-r, r+1):
            x2 = x*x
            for y in range(-r, r+1):
                if (x2 + y**2) > r2:
                    continue
                new_loc = self.distant_location(loc, x, y)
                obj = self.data[new_loc][0]
                score = self.heuristic_score.get(obj, 0)
                #getLogger().debug("Heuristic for location (%d, %d): %d" % (new_loc/self.cols, new_loc%self.cols, score))
                # North direction
                if (x + y >= 0) and (x - y < 0):
                    scores[0] += score
                    if obj & JACKPOT:
                        weights[0] = 100
                    #getLogger().debug("Adding score to North")
                # West direction
                elif (x - y <= 0) and (x + y < 0):
                    scores[1] += score
                    if obj & JACKPOT:
                        weights[1] = 100
                    #getLogger().debug("Adding score to West")
                # South direction
                elif (x + y <= 0) and (x - y > 0):
                    scores[2] += score
                    if obj & JACKPOT:
                        weights[2] = 100
                    #getLogger().debug("Adding score to South")
                # East direction
                else:
                    scores[3] += score
                    if obj & JACKPOT:
                        weights[3] = 100
                    #getLogger().debug("Adding score to East")
        #getLogger().debug("JACKPOT: %s" % str(weights))
        #scores = [weights[i]*scores[i] for i in range(4)]
        score_dict = {}
        dirs = ('n', 'w', 's', 'e')
        for score, weight, dir in zip(scores, weights, dirs):
            if (score > self.heuristic_score[MY_ANT]) and (score <= 6*self.heuristic_score[MY_ANT]):
                score_dict[dir] = score*weight + 100
            else:
                score_dict[dir] = score*weight
        del weights[:]
        del scores[:]
        return score_dict
            
    def attack(self, loc):
        #score_dict = self.compute_dir_scores(loc)
        score_dict = self.get_battle_scores(loc)
        dir_order = sorted(score_dict.keys(), key=score_dict.get, reverse=True)
        getLogger().debug("Computed order: %s" % score_dict)
        if score_dict[dir_order[0]] > score_dict[dir_order[-1]]:
            for dir in dir_order:
                if self.do_move(loc, dir):
                    self.my_ants[loc].state = ATTACKING
                    self.my_ants[loc].prev_dir = dir
                    return True

    def explore(self, loc):
        next_dirs = {
                'n' : (('w', 'e', 's'), ('e', 'w', 's')),
                's' : (('w', 'e', 'n'), ('e', 'w', 'n')),
                'e' : (('n', 's', 'w'), ('s', 'n', 'w')),
                'w' : (('n', 's', 'e'), ('s', 'n', 'w'))
                }
        #getLogger().debug("Ant (%d, %d) ready to explore the world" % (loc/self.cols, loc%self.cols))
        dir = self.my_ants[loc].prev_dir
        if not dir:
            dir = ('n', 's', 'e', 'w')[random.randint(0, 3)]
        #getLogger().debug("Trying to move %s" % dir)
        if self.do_move(loc, dir):
            self.my_ants[loc].state = EXPLORING
            self.my_ants[loc].prev_dir = dir
            return True
        dirs = next_dirs[dir][random.randint(0, 1)]
        for dir in dirs:
            #getLogger().debug("Trying to move %s" % dir)
            if self.do_move(loc, dir):
                self.my_ants[loc].state = EXPLORING
                self.my_ants[loc].prev_dir = dir
                return True
        return False


    def gather(self, loc):
        directions = list(ANTICLOCKWISE)
        best_val = self.food_rewards.get(loc, self.turn)
        best_dir = None
        #getLogger().debug("Scanning best direction for ant (%d, %d)" % (loc/self.cols, loc%self.cols))
        rewards = dict([(i, self.food_rewards.get(self.new_loc(loc, i), 0)) for i in directions])
        #getLogger().debug("rewards: %s" % str(rewards))
        rewards = {'n' : 0, 'w' : 0, 's' : 0, 'e' : 0}
        #battle_scores = self.get_battle_scores(loc)
        for i in directions:
            reward = self.food_rewards.get(self.new_loc(loc, i), 0)
            #reward += battle_scores[i]
            rewards[i] = reward

        directions.sort(key=rewards.get, reverse=True)
        if rewards[directions[0]] == rewards[directions[-1]]:
            return False
        for best_dir in directions:
            if self.do_move(loc, best_dir):
                self.my_ants[loc].state = GATHERING
                self.available_ants.pop(loc)
                self.my_ants[loc].prev_dir = best_dir
                return True
        return False

    def update_food(self):
        #getLogger().debug("old_food: %s, new_food %s" % (self.old_food, self.food))
        self.food_rewards.clear()
        for loc in self.enemy_hills:
            self.populate_max_qvalues(loc, 4*self.scan_radius, 0)
        for loc in self.food:
            self.populate_max_qvalues(loc, self.scan_radius, 0)

    def update_visible(self):
        r = self.viewradius
        for loc in self.my_ants:
            for x in range(-r, r+1):
                for y in range(-r, r+1):
                    if (x**2 + y**2) <= self.viewradius2:
                        new_loc = self.distant_location(loc, x, y)
                        if self.data[new_loc][0] == UNSEEN:
                            self.data[new_loc][0] = LAND

    def do_turn(self):
        for hill in self.enemy_hills[:]:
            if self.orders.has_key(hill):
                if not self.dead_ants.has_key(hill):
                    self.enemy_hills.remove(hill)
                    self.data[hill][0] = LAND
        self.dead_ants.clear()

        self.orders.clear()
        self.available_ants.update(self.my_ants)
        self.update_visible()
        self.update_food()
        for ant in self.available_ants.keys():
            if self.gather(ant):
                continue
            #if self.attack(ant):
            #    continue
            self.explore(ant)
        self.cleanup()
