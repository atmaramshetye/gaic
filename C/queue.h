#ifndef _QUEUE_H_
#define _QUEUE_H_

struct queue_node {
    void *data;
    struct queue_node *next;
};

struct queue {
    struct queue_node *head;
    struct queue_node *tail;
    int length;
    unsigned char should_dealloc;
};

struct queue *create_queue(unsigned char should_dealloc);
void push(struct queue *q, void *data);
void *pull(struct queue *q);
void clear_queue(struct queue *q);

#endif
