#include <assert.h>
#include "queue.h"

struct queue *create_queue(unsigned char should_dealloc) {
    struct queue *q = (struct queue *)malloc(sizeof(struct queue));
    struct queue_node *qn = (struct queue_node *)malloc(sizeof(struct queue_node *));

    qn->data = 0;
    qn->next = 0;

    q->head = qn;
    q->tail = qn;
    q->length = 0;
    q->should_dealloc = should_dealloc;

    return q;
}

void push(struct queue *q, void *data) {
    struct queue_node *qn = (struct queue_node *)malloc(sizeof(struct queue_node *));

    qn->data = data;
    qn->next = q->tail->next;
    q->tail->next = qn;
    q->tail = qn;
    q->length += 1;
}

void *pull(struct queue *q) {
    struct queue_node *qn = q->head->next;
    assert(qn);
    if (!qn)
        return 0;
    void *data = qn->data;
    q->head->next = qn->next;
    qn->next = 0;
    qn->data = 0;
    free(qn);
    q->length -= 1;
    return data;
}

void clear_queue(struct queue *q) {
    struct queue_node *qn = q->head->next;
    struct queue_node *temp;
    while (qn) {
        temp = qn;
        q->head->next = qn->next;
        if (q->should_dealloc) 
            free(qn->data);
        qn = qn->next;
        temp->next = 0;
        free(temp);
    }
    q->length = 0;
}

#ifdef TEST_QUEUE
int main(int argc, char *argv) {
    struct queue *q = create_queue(0);
    push(q, 1);
    push(q, 2);
    push(q, 3);
    push(q, 4);
    assert((int)pull(q) == 1);
    assert((int)pull(q) == 2);
    assert((int)pull(q) == 3);
    assert((int)pull(q) == 4);

    return(0);
}
#endif
