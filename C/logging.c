#include<stdio.h>
#include<stdarg.h>
#include<assert.h>

void log_debug(char *fmt, ...) {
#ifdef LOG_DEBUG
    FILE *fp;
    va_list args;

    assert(fmt);
    va_start(args, fmt);
    fp = fopen("/tmp/ants.log", "a");
    assert(fp);
    vfprintf(fp, fmt, args);
    va_end(args);
    fclose(fp);
#endif
}
