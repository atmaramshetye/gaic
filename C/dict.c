#include <string.h>
#include "dict.h"

struct dict *create_dict(unsigned int max_size, unsigned char dealloc_data) {
    struct dict *m = (struct dict *)malloc(sizeof(struct dict));
    struct dict_node *n = (struct dict_node *)malloc(sizeof(struct dict_node));

    n->data = 0;
    n->next = 0;

    m->keys = (struct dict_node **)calloc(max_size, sizeof(struct dict_node *));
    m->values = n;
    m->tail = n;
    m->max_size = max_size;
    m->dealloc_data = dealloc_data;
    m->length = 0;
    return m;
}

unsigned char has_key(struct dict *m, unsigned int key) {
    return (m->keys[key] != 0);
}

void *pop(struct dict *m, unsigned int key) {
    struct dict_node *n = m->keys[key];
    struct dict_node *prev;
    void *data;

    if (!n)
        return 0;

    prev = n->prev;
    data = n->data;
    prev->next = n->next;
    if (n->next) {
        n->next->prev = prev;
    }

    free(n);
    m->keys[key] = 0;
    m->length -= 1;

    return data;
}

void *get(struct dict *m, unsigned int key) {
    struct dict_node *n = m->keys[key];
    void *data;

    if (!n)
        return 0;

    return n->data;
}

void set(struct dict *m, unsigned int key, void *data) {
    struct dict_node *n = m->keys[key];

    if (!n) {
        n = (struct dict_node *)malloc(sizeof(struct dict_node));
        n->next = 0;
        m->keys[key] = n;
        n->prev = m->tail;
        m->tail->next = n;
        m->tail = n;
        m->length += 1;
    } else if (m->dealloc_data) {
        free(n->data);
    }
    n->data = data;
}

void clear(struct dict *m) {
    /* This function might need optimizations */
    struct dict_node *n = m->values->next;
    struct dict_node *temp;
    while (n) {
        temp = n;
        n = n->next;
        if (m->dealloc_data)
            free(temp->data);
        temp->next = 0;
        free(temp);
    }
    /* If memset becomes a bottleneck, then the keys might also need to be stored */
    memset(m->keys, 0, m->max_size);
    m->length = 0;
}

#ifdef TEST_MAP
#include <assert.h>

void print_list(struct dict *m, const char *format) {
    struct dict_node *n = m->values->next;
    while (n) {
        char buf[100];
        sprintf(buf, "Data: %s, ", format);
        printf(buf, n->data);
        n = n->next;
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    struct dict *m = create_dict(10, 0);
    int i;

    set(m, 1, (void *)1);
    set(m, 2, (void *)4);
    set(m, 3, (void *)9);
    set(m, 4, (void *)16);
    print_list(m, "%d");
    assert((int)get(m, (void *)1) == 1);
    assert((int)get(m, (void *)2) == 4);
    assert((int)get(m, (void *)3) == 9);
    assert((int)get(m, (void *)4) == 16);
    assert((int)get(m, (void *)5) == 0);
    assert((int)pop(m, (void *)1) == 1);
    print_list(m, "%d");
    assert((int)pop(m, (void *)2) == 4);
    assert((int)pop(m, (void *)4) == 16);
    assert((int)get(m, (void *)1) == 0);
    clear(m);

    m = create_dict(10, 1);
    for(i = 0; i < 5; i++) {
        char * foo = (char *)malloc(10*sizeof(char));
        sprintf(foo, "atma%d", i);
        printf("%s\n", foo);
        set(m, i, foo);
    }
    print_list(m, "%s");

    assert(!strcmp((char *)get(m, 0), "atma0"));
    assert(!strcmp((char *)get(m, 1), "atma1"));
    assert(!strcmp((char *)get(m, 2), "atma2"));
    assert(!strcmp((char *)get(m, 3), "atma3"));
    assert(!strcmp((char *)pop(m, 3), "atma3"));
    print_list(m, "%s");
    assert(pop(m, 3) == 0);
    print_list(m, "%s");
    clear(m);

    return (0);
}
#endif
