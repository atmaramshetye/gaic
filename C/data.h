#ifndef _DATA_H_
#define _DATA_H_

#define ENEMY_HILL 4
#define MY_HILL 8
#define FOOD 16
#define WATER 32
#define MY_ANT 64
#define ENEMY_ANT 64
#define MY_DEAD_ANT 64
#define ENEMY_DEAD_ANT 64

struct game_info {
	int loadtime;
	int turntime;
	int rows;
	int cols;
	int turns;
	int viewradius2;
    int viewradius;
	int attackradius2;
	int attackradius;
	int spawnradius2;
    int seed;
};

struct game_data {
	int *map;
    int turn;
};

#endif
