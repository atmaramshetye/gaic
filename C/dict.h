#ifndef _DICT_H_
#define _DICT_H_

/* Implementation of a fast array and list based dict.
   The dict is limited by size and the speed comes at a cost of 
   a lot of memory wastage.

   The use case for this type of a structure is when one needs to search data
   dictped with integer keys and the number of keys is dynamic */

#include<stdlib.h>

struct dict_node {
    void *data;
    struct dict_node *next;
    struct dict_node *prev;
};

struct dict {
    struct dict_node *values;
    struct dict_node **keys;
    int length;
    int max_size;
    struct dict_node *tail;
    unsigned char dealloc_data;
};

struct dict *create_dict(unsigned int max_size, unsigned char dealloc_data);
unsigned char has_key(struct dict *m, unsigned int key);
void *pop(struct dict *m, unsigned int key);   /* returns 0 on not found */
void *get(struct dict *m, unsigned int key);   /* returns 0 on not found */
void set(struct dict *m, unsigned int key, void *data);
void clear(struct dict *m);

#endif
