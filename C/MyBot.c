#include <stdio.h>
#include "data.h"

void end_turn(void) {
    fprintf(stdout, "go\n");
    fflush(stdout);
}

void setup(struct game_info *info) {
    char buf[1000];
    while (1) {
        fgets(buf, 1000, stdin);
        log_debug(buf);
        if (!strncmp(buf, "ready", 5))
            break;

        if (!strncmp(buf, "cols ", 5)) {
            info->cols = atoi(buf+5);
        } else if (!strncmp(buf, "rows ", 5)) {
            info->rows = atoi(buf+5);
        } else if (!strncmp(buf, "player_seed ", 12)) {
            info->seed = atoi(buf+12);
        } else if (!strncmp(buf, "turntime ", 9)) {
            info->turntime = atoi(buf+9);
        } else if (!strncmp(buf, "loadtime ", 9)) {
            info->loadtime = atoi(buf+9);
        } else if (!strncmp(buf, "viewradius2 ", 12)) {
            info->viewradius2 = atoi(buf+9);
        } else if (!strncmp(buf, "attackradius2 ", 14)) {
            info->attackradius2 = atoi(buf+14);
        } else if (!strncmp(buf, "spawnradius2 ", 13)) {
            info->spawnradius2 = atoi(buf+13);
        } else if (!strncmp(buf, "turns ", 6)) {
            info->turns = atoi(buf+6);
        }
    }

    //setup_engine();
    end_turn();
}

void update(struct game_info *info, struct game_data *data) {
    char buf[1000];
    int idx;
    char *tmp;
    int offsets[5];
    int i = 0, j = 0;
    int r, c;
    int player;

    while (1) {
        fgets(buf, 1000, stdin);
        if (!strncmp(buf, "go", 2))
            break;

        while (*(buf+i)) {
            if (*(buf+i) == ' ') {
                *(buf+i) = 0;
                offsets[j] = i+1;
                j++;
            }
            i += 1;
        }

        switch(buf[0]) {
            case 'w':
                r = atoi(buf + offsets[0]);
                c = atoi(buf + offsets[1]);
                idx = r*info->cols + c;
                data->map[idx] = WATER;
                break;
            case 'a':
                r = atoi(buf + offsets[0]);
                c = atoi(buf + offsets[1]);
                idx = r*info->cols + c;
                player = atoi(buf + offsets[2]);
                if (player)
                    data->map[idx] = MY_ANT;
                else
                    data->map[idx] = ENEMY_ANT;
                break;
            case 'd':
                r = atoi(buf + offsets[0]);
                c = atoi(buf + offsets[1]);
                idx = r*info->cols + c;
                player = atoi(buf + offsets[2]);
                if (player)
                    data->map[idx] = MY_DEAD_ANT;
                else
                    data->map[idx] = ENEMY_DEAD_ANT;
                break;
            case 'f':
                r = atoi(buf + offsets[0]);
                c = atoi(buf + offsets[1]);
                idx = r*info->cols + c;
                data->map[idx] = FOOD;
                break;
            case 'h':
                r = atoi(buf + offsets[0]);
                c = atoi(buf + offsets[1]);
                idx = r*info->cols + c;
                player = atoi(buf + offsets[2]);
                if (player)
                    data->map[idx] = MY_HILL;
                else
                    data->map[idx] = ENEMY_HILL;
                break;
            default:
                ;
        }
    }
}

int main(int argc, char *argv) {
    struct game_info info;
    struct game_data data;

    setup(&info);

    data.map = (int *)malloc(info.rows * info.cols);

    while (!feof(stdin)) {
        update(&info, &data);
        do_turn(&info, &data);
        end_turn();
    }
    return(0);
}
